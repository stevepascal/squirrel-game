package com.spascal.game.ui_and_threads;

/**
 * Created by S on 1/15/2018.
 */


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

import com.spascal.game.R;

public class StartMenu extends Activity implements View.OnClickListener {

    ImageButton startButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.start_menu);
        startButton = (ImageButton) findViewById(R.id.player1);
        startButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.player1:
                Intent playervscp = new Intent(StartMenu.this, Game.class);
                playervscp.putExtra("players", false);
                startActivity(playervscp);
                break;
            default:
                break;
        }

    }
}
