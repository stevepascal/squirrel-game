package com.spascal.game.ui_and_threads;

/**
 * Created by S on 1/15/2018.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.spascal.game.R;
import com.spascal.game.math.Vector2;
import com.spascal.game.shapes.BackgroundSky;
import com.spascal.game.shapes.CloudGroup;
import com.spascal.game.shapes.GameOverWindow;
import com.spascal.game.shapes.Grass;
import com.spascal.game.shapes.Player;
import com.spascal.game.shapes.RandomObject;

import java.lang.Thread.State;


public class GameView extends SurfaceView implements SurfaceHolder.Callback {


    //I don't know why they need to be here, but they are here
    private Context context;
    private GameThread thread;
    private Handler handler;
    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);


        SurfaceHolder holder = getHolder();
        holder.addCallback(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message m) {

                Bundle b = m.getData();
                MotionEvent e = b.getParcelable("event");
                thread.handleInput(e);
            }
        };

        thread = new GameThread(holder, context, handler);
        setFocusable(true);
    }

    public GameThread getThread() {
        return thread;
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        if (!hasWindowFocus)
            thread.pause();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
        thread.setSurfaceSize(width, height);
    }

    public void surfaceCreated(SurfaceHolder holder) {

        if (thread.getState() == State.TERMINATED) {
            thread = new GameThread(getHolder(), context, handler);
            thread.setRunning(true);
            thread.start();
            thread.doStart();
        } else {
            thread.setRunning(true);
            thread.start();
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {

        boolean retry = true;
        thread.setRunning(false);
        while (retry) {
            try {

                thread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        Message m = handler.obtainMessage();

        Bundle bundle = new Bundle();
        bundle.putParcelable("event", event);

        m.setData(bundle);
        handler.sendMessage(m);

        return true;

    }

    class GameThread extends Thread {

        //states
        public static final int STATE_LOSE = 1;
        public static final int STATE_PAUSE = 2;
        public static final int STATE_READY = 3;
        public static final int STATE_RUNNING = 4;
        static final int WINPOINT = 3;
        //shapes
        private Paint paint;
        private Player player;
        private BackgroundSky backgroundSky;
        private Grass grass;
        private CloudGroup cloudGroup0;
        private CloudGroup cloudGroup1;
        private CloudGroup cloudGroup2;
        private CloudGroup cloudGroup3;
        private CloudGroup cloudGroup4;
        private RandomObject randomObject0;
        private GameOverWindow gameOverWindow;

        //user interface class
       // private UserInterface userInterface;
        //canvas dimensions
        private int canvasWidth;
        private int canvasHeight;
        private long lastTime;
        private int mode = STATE_RUNNING;
        //idk
        private boolean run = false;
        // Handle to the surface manager
        private SurfaceHolder surfaceHolder;


        public GameThread(SurfaceHolder surfaceHolder, Context contextInput, Handler handlerInput) {

            this.surfaceHolder = surfaceHolder;
            context = contextInput;
            handler = handlerInput;

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            canvasWidth = metrics.widthPixels;
            canvasHeight = metrics.heightPixels;

            paint = new Paint();
            paint.setTextSize(getResources().getDimensionPixelSize(R.dimen.myFontSize));


            createShapes();
           // userInterface = new UserInterface(context, canvasWidth, canvasHeight);
        }

        private void createShapes(){
            backgroundSky = new BackgroundSky(canvasHeight, canvasWidth);
            grass = new Grass(canvasHeight, canvasWidth);

            //cloud list
            cloudGroup0 = new CloudGroup(canvasHeight, canvasWidth, context, 0);
            cloudGroup1 = new CloudGroup(canvasHeight, canvasWidth, context, 1000);
            cloudGroup2 = new CloudGroup(canvasHeight, canvasWidth, context, 2000);
            cloudGroup3 = new CloudGroup(canvasHeight, canvasWidth, context, -1000);
            cloudGroup4 = new CloudGroup(canvasHeight, canvasWidth, context, 3000);

            randomObject0 = new RandomObject(canvasHeight, canvasWidth, context);

            gameOverWindow = new GameOverWindow(canvasHeight, canvasWidth, context, R.drawable.game_over);

            player = new Player(context, new Vector2(100, canvasHeight - 225));
        }

        private void startShapes(){
            cloudGroup0.startClouds();
            cloudGroup1.startClouds();
            cloudGroup2.startClouds();
            cloudGroup3.startClouds();
            cloudGroup4.startClouds();
             randomObject0.start();
        }

        //initalise the game
        public void doStart() {
            synchronized (surfaceHolder) {

                resetGame();
                lastTime = System.currentTimeMillis() + 100;
                setState(STATE_RUNNING);
            }
        }

        public void pause() {
            synchronized (surfaceHolder) {
                if (mode == STATE_RUNNING)
                    setState(STATE_PAUSE);
            }
        }

        @Override
        public void run() {

            while (run) {
                Canvas c = null;
                try {
                    c = surfaceHolder.lockCanvas(null);
                    synchronized (surfaceHolder) {
                        if (mode == STATE_RUNNING) {
//                            System.out.println("GameView#run/update");
                            updateGame();
                        }
                        doDraw(c);
                    }
                } catch (Exception e) {
                } finally {

                    if (c != null) {
                        surfaceHolder.unlockCanvasAndPost(c);
                    }
                }
            }
        }

        public void setRunning(boolean b) {
            run = b;
        }


        public void setState(int mode) {
            synchronized (surfaceHolder) {
                setState(mode, null);
            }
        }


        public void setState(int mode, CharSequence message) {
            synchronized (surfaceHolder) {
                mode = mode;
            }
        }

        public void setSurfaceSize(int width, int height) {

            synchronized (surfaceHolder) {
                canvasWidth = width;
                canvasHeight = height;
            }
        }


        public void unpause() {

            synchronized (surfaceHolder) {
                lastTime = System.currentTimeMillis() + 100;
            }
            setState(STATE_RUNNING);
        }


        private void doDraw(Canvas canvas) {

            backgroundSky.drawBackgroundSky(canvas, paint);
            grass.drawGrass(canvas, paint);

            cloudGroup0.draw(canvas, paint);
            cloudGroup1.draw(canvas, paint);
            cloudGroup2.draw(canvas, paint);
            cloudGroup3.draw(canvas, paint);
            cloudGroup4.draw(canvas, paint);

            if (player.isAlive()) {


                randomObject0.drawBitMap(canvas, paint);
                player.drawPlayer(canvas, paint);

                paint.setColor(Color.BLACK);
                canvas.drawText(String.valueOf(player.getScore()), 100.0f, 250.0f, paint);

            } else { //TODO switch "splash" to "score/continue"
                gameOverWindow.drawBitMap(canvas, paint);
                thread.pause(); //TODO maybe not?
            }

            if (mode == STATE_READY) {
                //canvas.drawText("Start game through Menu", canvasWidth / 3, canvasHeight / 3, paint);
            }

        }

        private void updateGame() {

            long now = System.currentTimeMillis();

            if (lastTime > now)
                return;
            double elapsed = (now - lastTime) / 10.0;
            lastTime = now;

            if (player.isAlive()) {
                //userInterface.updateScore(true);//true for player 1//TODO pick one of these two lines ^^^^

                player.updateJump(elapsed);

                handleCloudGroups(elapsed);

                randomObject0.updatePositionX(elapsed);

            }

//            TODO loop all shown objects?, create list for "bad"/collidable objects?
            if (collided(randomObject0.getRectangle())) {
                System.out.println("collision detected!");
                player.setAlive(false);
            }

            /*
            if(earnedPoint(randomObject0.getRectangle())){
                System.out.println("Earned Point!");
                scoreboard.addToScore(1);
            }*/

        }

        private void handleCloudGroups(double elapsed) {
            cloudGroup0.updatePosition(elapsed);
            cloudGroup1.updatePosition(elapsed);
            cloudGroup2.updatePosition(elapsed);
            cloudGroup3.updatePosition(elapsed);
            cloudGroup4.updatePosition(elapsed);
        }

        /*
        * Player earns points for clearing objects
         */
        private boolean earnedPoint(Rect rectangle) {

            float bx = player.getPosition().x;
            float rr = rectangle.right;

            if ( bx <= rr) {
                return true;
            }

            return false;
        }

        //TODO update inner variable names & fix
        private boolean collided(Rect rectangle) {

            float playerPositionX = player.getPosition().x;
            float playerPositionY = player.getPosition().y;
            float size = player.getSize();
            float rectangleLeft = rectangle.left;
            float rectangleRight = rectangle.right;
            float rectangleTop = rectangle.top;
            float rectangleBottom = rectangle.bottom;

            if (playerPositionY  >= rectangleTop &&
                    playerPositionX >= rectangleLeft &&
                    playerPositionX + size <= rectangleRight &&
                    playerPositionY + size <= rectangleBottom ) {
                collidedDebugMessages(rectangle);
                return true;
            }

            return false;
        }

        private void collidedDebugMessages(Rect rectangle){
            float playerPositionX = player.getPosition().x;
            float playerPositionY = player.getPosition().y;
            float size = player.getSize();
            float rectangleLeft = rectangle.left;
            float rectangleRight = rectangle.right;
            float rectangleTop = rectangle.top;
            float rectangleBottom = rectangle.bottom;

            System.out.println("playerPositionX : " + playerPositionX);
            System.out.println("playerPositionY : " + playerPositionY);
            System.out.println("size : " + size);
            System.out.println("rectangleTop : " + rectangleTop);
            System.out.println("rectangleLeft : " + rectangleLeft);
            System.out.println("rectangleRight : " + rectangleRight);
            System.out.println("rectangleBottom : " + rectangleBottom);
        }

        public void resetGame() {

            backgroundSky.resetBackground(new Vector2(canvasWidth, canvasHeight));
            grass.resetBackground(new Vector2(canvasWidth, canvasHeight));
            player.resetPlayer();
            randomObject0.reset();

            setState(STATE_RUNNING);
        }

        public void handleInput(MotionEvent event) {

            InputHandler ih = InputHandler.getInstance();

            for (int i = 0; i < ih.getTouchCount(event); i++) {

                int x = (int) ih.getX(event, i);
                int y = (int) ih.getY(event, i);

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        player.setPressedDown(true);

                        if (player.isAlive() & !player.isAirborne()) {
                            player.beginJump();
                            startShapes();

                        } else if(!player.isAlive() && gameOverWindow.getHandle().contains(x, y)){
                            thread.doStart();
                        }

                        break;
                    case MotionEvent.ACTION_MOVE:

                        break;
                    case MotionEvent.ACTION_UP:

                        player.setPressedDown(false);
                        player.setFalling();

                        break;

                }
            }
        }

    }
}
