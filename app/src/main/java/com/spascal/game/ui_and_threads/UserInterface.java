package com.spascal.game.ui_and_threads;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.spascal.game.R;

public class UserInterface {

    private int canvasHeight;
    private int canvasWidth;
    private boolean showGameOver;


    public UserInterface(Context context, int width, int height) {
        canvasHeight = height;
        canvasWidth = width;
        showGameOver = false;
    }


}
