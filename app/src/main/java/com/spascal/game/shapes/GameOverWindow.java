package com.spascal.game.shapes;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
/**
 * Created by S on 2/17/2018.
 */

public class GameOverWindow extends MyShape {
    private Rect handle = new Rect();

    public GameOverWindow(int canvasHeight, int canvasWidth, Context context, int resourceId) {
        super(canvasHeight, canvasWidth, context, resourceId);

        handle = new Rect();
        texture = BitmapFactory.decodeResource(context.getResources(), resourceId);
        position.set((canvasWidth/2) - (texture.getWidth()/2) , (canvasHeight/2) - (texture.getHeight()/2));
    }

    public Rect getHandle() {
        // left,  top,  right,  bottom
        handle.set(position.x, position.y, (position.x + texture.getWidth()), (position.y + texture.getHeight()));
        return handle;
    }

}
