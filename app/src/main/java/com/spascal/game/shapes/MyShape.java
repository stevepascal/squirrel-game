package com.spascal.game.shapes;

/**
 * Created by S on 1/15/2018.
 */


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

import com.spascal.game.math.Vector2;

import java.util.Random;

public class MyShape {

    //simple color shapes
    public int color;
    //position
    protected Vector2 position;

    //velocity
    protected Vector2 velocity;

    //background speed
    protected float speed;

    //size
    protected int canvasWidth;
    protected int canvasHeight;
    //images
    protected Bitmap texture;

    protected int MIN_RANDOM = 0;
    protected int MAX_RANDOM = 100;

    protected boolean go = false;

    protected Rect rectangle = new Rect();

    protected Vector2 startingPosition = null;


    public MyShape(Vector2 pos, int canvasHeight, int canvasWidth, int colorInput) {
        color = colorInput;
        setVelocity(new Vector2());
        setPosition(new Vector2());

        this.canvasHeight = canvasHeight;
        this.canvasWidth = canvasWidth;

    }

    public MyShape(int canvasHeight, int canvasWidth) {
        setVelocity(new Vector2());
        setPosition(new Vector2());

        this.canvasHeight = canvasHeight;
        this.canvasWidth = canvasWidth;

    }

    public MyShape(int canvasHeight, int canvasWidth, Context context, int resourceId) {
        setVelocity(new Vector2());
        setPosition(new Vector2());

        this.canvasHeight = canvasHeight;
        this.canvasWidth = canvasWidth;
    }

    public void resetBackground(Vector2 start) {
        position.x = (0);
        position.y = (0);

        setVelocity(1);
    }

    public void setPosition(int x, int y) {
        position.set(x, y);
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 pos) {
        if(startingPosition == null ){
            startingPosition = pos;
        }
        this.position = pos;
    }

    public void update(double elapsed) {
        position.y += velocity.y * speed * elapsed;

    }

    public Vector2 getVelocity() {
        return velocity;
    }

    public void setVelocity(int x) {
        this.velocity.x = x;
    }

    public void inverseX() {
        velocity.x = -velocity.x;
    }

    public void inverseY() {
        velocity.y = -velocity.y;
    }

    public void setVelocity(Vector2 velocity) {
        this.velocity = velocity;
    }

    public void drawRectangle(Canvas canvas, Paint paint) {
        Rect rectangle = new Rect(0, 0, canvasHeight, canvasWidth);

        paint.setColor(color);

        canvas.drawRect(rectangle, paint);
    }

    public void drawBitMap(Canvas canvas, Paint paint) {
        canvas.drawBitmap(texture, this.position.x, this.position.y, paint);
    }

    public void drawOval(Canvas canvas, Paint paint) {
        //  OvalShape oval = new OvalShape();
        //TODO randomize shapes?
        RectF oval = new RectF(300, 300, 200, 100);
        paint.setColor(color);

        canvas.drawOval(oval, paint);
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }


    /*
    * This will return a location for 100 percentiles
    */
    protected int getRandomHeight(int min, int max){
        int randomNumber = getRandomNumber(min, max);

        return  canvasHeight - ( randomNumber * (canvasHeight/100 ) );
    }

    protected int getRandomNumber(int min, int max){
        Random random = new Random();

        return random.nextInt((max - min) + 1) + min;
    }

    public void updatePositionX(double elapsed) {
        if (go) {
            this.position.x -= velocity.x * speed * elapsed;

            //reset
            if (this.position.x < - 800) {
                this.position.x = canvasWidth  + 800;

                //position.y = getRandomHeight(MIN_RANDOM, MAX_RANDOM);
            }
        }

        update();
    }

    public Rect getRectangle() {
        return rectangle;
    }

    public void setRectangle(Rect rectangle) {
        this.rectangle = rectangle;
    }

    public void update() {
        this.rectangle.set(position.x, position.y, (position.x + texture.getWidth()), (position.y + texture.getHeight()));
    }

    public void reset(){
        setPosition(startingPosition);
    }
}