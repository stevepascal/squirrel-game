package com.spascal.game.shapes;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;

import com.spascal.game.R;
import com.spascal.game.math.Vector2;
/**
 * Created by S on 2/8/2018.
 */

public class Cloud extends MyShape {

    public Cloud(int canvasHeight, int canvasWidth, Context context, int seed) {
        super(canvasHeight, canvasWidth);
        color = Color.WHITE;

        Vector2 velocity = new Vector2();
        velocity.set(3, 0);
        setVelocity(velocity);
        setSpeed(1.0f);

        this.canvasHeight = canvasHeight;
        this.canvasWidth = canvasWidth;

        MIN_RANDOM = 88;
        MAX_RANDOM = 99;

        texture = BitmapFactory.decodeResource(context.getResources(), getRandomObjectId());

        //TODO randomize a bit?
        setPosition(0 + seed, getRandomHeight(MIN_RANDOM, MAX_RANDOM));
    }

    //TODO add clear "clouds" to act as spacers?
    private int getRandomObjectId(){
        int randomResourceObjectId;
        int randomNumber = getRandomNumber(0, 5);

        if (randomNumber == 0) {
            randomResourceObjectId = R.drawable.cloud3;
        } else if (randomNumber == 1) {
            randomResourceObjectId = R.drawable.cloud2;
        } else {
            randomResourceObjectId = R.drawable.cloud1;
        }

        return randomResourceObjectId;
    }

    private void debugUpdateMessages(double elapsed) {
        System.out.println("Cloud#debugUpdateMessages : BEGIN : ");
        System.out.println("Cloud#debugUpdateMessages : position.y : " + position.x);
        System.out.println("Cloud#debugUpdateMessages : velocity.y : " + velocity.x);
        System.out.println("Cloud#debugUpdateMessages : speed : " + speed);
        System.out.println("Cloud#debugUpdateMessages : elapsed : " + elapsed);
        double proposedDeltaMove = velocity.y * speed * elapsed;
        System.out.println("Cloud#debugUpdateMessages : proposedDeltaMove : " + proposedDeltaMove);
        System.out.println("Cloud#debugUpdateMessages : END : ");
    }
}
