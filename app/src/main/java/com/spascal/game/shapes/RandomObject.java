package com.spascal.game.shapes;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;

import com.spascal.game.R;
import com.spascal.game.math.Vector2;
/**
 * Created by S on 2/8/2018.
 */

public class RandomObject extends MyShape {


    public RandomObject(int canvasHeight, int canvasWidth, Context context) {
        super(canvasHeight, canvasWidth);
        color = Color.WHITE;

        Vector2 velocity = new Vector2();
        velocity.set(2, 0);
        setVelocity(velocity);
        setSpeed(1.5f);

        this.canvasHeight = canvasHeight;
        this.canvasWidth = canvasWidth;

        MIN_RANDOM = 10;
        MAX_RANDOM = 90;

        texture = BitmapFactory.decodeResource(context.getResources(), getRandomObjectId());

        //TODO randomize a bit?
        // setPosition(new Vector2(-500, getRandomHeight(MIN_RANDOM, MAX_RANDOM)));
         setPosition(new Vector2(-500, canvasHeight - 250)); //TODO this is tied to squirrel starting height btw
    }

    private int getRandomObjectId(){
        int randomResourceObjectId = 0;
        int randomNumber = getRandomNumber(0, 4);

        if (randomNumber == 0) {
            randomResourceObjectId = R.drawable.random_object1;
        } else if (randomNumber == 1) {
            randomResourceObjectId = R.drawable.random_object1;
        } else if (randomNumber == 2) {
            randomResourceObjectId = R.drawable.random_object1;

        } else {
            randomResourceObjectId = R.drawable.random_object1;
        }

        return randomResourceObjectId;
    }

    public void start() {
        go = true;
    }

}
