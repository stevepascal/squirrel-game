package com.spascal.game.shapes;

/**
 * Created by S on 1/15/2018.
 */


import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import com.spascal.game.math.Vector2;

public class Grass extends MyShape {

    public Grass(int canvasHeight, int canvasWidth) {
        super(canvasHeight, canvasWidth);
        color = Color.GREEN;

        this.canvasHeight = canvasHeight;
        this.canvasWidth = canvasWidth;

        setVelocity(0);
        setSpeed(0);
        setPosition(0,0);

    }

    public void drawGrass(Canvas canvas, Paint paint) {
        Rect rectangle = new Rect(0, canvasHeight-100, canvasWidth+200, canvasHeight);

        paint.setColor(color);

        canvas.drawRect(rectangle, paint);
    }
}

