package com.spascal.game.shapes;

/**
 * Created by S on 1/15/2018.
 */


import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import com.spascal.game.math.Vector2;

public class BackgroundSky extends MyShape {

    public BackgroundSky(int canvasHeight, int canvasWidth) {
        super(canvasHeight, canvasWidth);
        color = Color.CYAN;

        setPosition(new Vector2(100 , 100));
    }



    public void drawBackgroundSky(Canvas canvas, Paint paint) {
        Rect rectangle = new Rect(0, 0, canvasWidth+200, canvasHeight);

        paint.setColor(color);

        canvas.drawRect(rectangle, paint);
    }
}

