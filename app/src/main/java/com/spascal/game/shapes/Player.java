package com.spascal.game.shapes;

/**
 * Created by S on 1/15/2018.
 */


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.widget.ImageView;

import com.spascal.game.R;
import com.spascal.game.math.Vector2;

public class Player {

    private final Vector2 STARTING_POSITION;
    private final float STARTING_POSITION_Y;
    private final int STARTING_VELOCITY_Y = 15;
    private final double ACCELERATION_Y_DELTA = 0.015000;
    private final double STARTING_ACCELERATION_Y = 1.0;

    private Vector2 position;
    private Vector2 velocity;
    private boolean isAlive = true;
    private int size = 50;
    private boolean isAirborne = false;
    private boolean isFalling = false;

    private int score;
    private int frame;
    private long lastScoreUpdateTime;
    private long lastFrameUpdateTime;
    private boolean pressedDown = false;
    private double acceleration_y;

    private Bitmap texture1;
    private Bitmap texture2;
    private Bitmap texture3;
    private Bitmap texture4;
    private Bitmap texture5;


    public Player(Context context, Vector2 positionInput) {
        velocity = new Vector2();
        texture1 = BitmapFactory.decodeResource(context.getResources(), R.drawable.squirrel1);
        texture2 = BitmapFactory.decodeResource(context.getResources(), R.drawable.squirrel2);
        texture3 = BitmapFactory.decodeResource(context.getResources(), R.drawable.squirrel3);
        texture4 = BitmapFactory.decodeResource(context.getResources(), R.drawable.squirrel4);
        texture5 = BitmapFactory.decodeResource(context.getResources(), R.drawable.squirrel5);
        STARTING_POSITION = positionInput;
        STARTING_POSITION_Y = STARTING_POSITION.y;
        lastScoreUpdateTime = 0l;
        score = 0;
        frame = 1;
        acceleration_y = STARTING_ACCELERATION_Y;

        setPosition(positionInput);
    }
    public void resetPlayer() {
        isFalling = false;
        isAirborne = false;
        velocity = new Vector2();
        lastScoreUpdateTime = 0l;
        acceleration_y = STARTING_ACCELERATION_Y;
        score = 0;
        frame = 1;
        setAlive(true);
        setPosition(STARTING_POSITION);
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 pos) {
        this.position = pos;
    }

    public void setPosition(int x, int y) {
        this.position.x = x;
        this.position.y = y;
    }

    public void drawPlayer(Canvas canvas, Paint paint) {
        long currentTime = System.currentTimeMillis();

        if(currentTime > lastFrameUpdateTime) {
            lastFrameUpdateTime = currentTime + 500l;
            frame++;
        }

        if(frame == 6)
            frame = 1;

        switch (frame){
            case 1:
                canvas.drawBitmap(texture1, position.x, position.y, paint);
                break;
            case 2:
                canvas.drawBitmap(texture2, position.x, position.y, paint);
                break;
            case 3:
                canvas.drawBitmap(texture3, position.x, position.y, paint);
                break;
            case 4:
                canvas.drawBitmap(texture4, position.x, position.y, paint);
                break;
            case 5:
                canvas.drawBitmap(texture5, position.x, position.y, paint);
                break;
        }

    }

    public void setVelocity(int x, int y) {
        this.velocity.x = x;
        this.velocity.y = y;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isAirborne() {
        return isAirborne;
    }

    public void setAirborne(boolean airborne) {
        isAirborne = airborne;
    }

    public void beginJump() {
        setAirborne(true);
        setVelocity(0, STARTING_VELOCITY_Y);
        acceleration_y = STARTING_ACCELERATION_Y;
    }

    //TODO break into multiple methods
    public void updateJump(double elapsed) {
        float position_y = position.y;

        if (isAirborne) {
            double proposedDeltaMove = getProposedDeltaMove(elapsed);

            if(isProposedMoveAboveGroundFloor(position_y, proposedDeltaMove)){
                updatePositionYWithProposedDeltaMove(proposedDeltaMove);
                updateAccelerationY();
            } else {
                position.set(position.x, (int) STARTING_POSITION_Y);
            }
        }
        checkForGrounded();
        updateScore();
    }

    private void updateAccelerationY(){
        acceleration_y -= ACCELERATION_Y_DELTA;
    }

    private void updatePositionYWithProposedDeltaMove(double proposedDeltaMove){
        position.y -= proposedDeltaMove;
    }

    private boolean isProposedMoveAboveGroundFloor(float position_y, double proposedDeltaMove){
        return ( position_y - proposedDeltaMove - STARTING_POSITION_Y ) < 0;
    }

    private double getProposedDeltaMove(double elapsed){
        return velocity.y * acceleration_y * elapsed;
    }

    private void checkForRestartJump(){
        if(!isAirborne && isPressedDown())
            beginJump();
    }

    private void checkForGrounded() {
        if (isAirborne) {

            float position_y = position.y; //for debugging
            float y_delta = position_y - STARTING_POSITION_Y;

            if (y_delta == 0) {

                isAirborne = false;
                isFalling = false;
                acceleration_y = STARTING_ACCELERATION_Y;
            }
        }

        checkForRestartJump();
    }

    public void setFalling() {
        if (!isFalling && isAirborne) {
            isFalling = true;
        }
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void updateScore(){
        long currentTime = System.currentTimeMillis();

        if(currentTime > lastScoreUpdateTime) {
            lastScoreUpdateTime = currentTime + 1000l;
            score++;
        }
    }

    public boolean isPressedDown() {
        return pressedDown;
    }

    public void setPressedDown(boolean pressedDown) {
        this.pressedDown = pressedDown;
    }

    //for debugging
    public void debugMEssages() {

        System.out.println("%%%%%%%%%%%%%% starting_y " + STARTING_POSITION_Y);
        System.out.println("player.update- acceleration y " + acceleration_y);
        System.out.println("player.update- position y " + position.y);
        System.out.println("player.update- velocity y " + velocity.y);
        System.out.println("player.update- isAirborne? " + isAirborne());
        System.out.println("player.update- isFalling? " + isFalling);

    }

}

