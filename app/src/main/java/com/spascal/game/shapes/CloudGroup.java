package com.spascal.game.shapes;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
/**
 * Created by S on 2/8/2018.
 */

public class CloudGroup {

    private Cloud[] clouds = new Cloud[12]; //how many (i think)

    public CloudGroup(int canvasHeight, int canvasWidth, Context context, int seed) {
        for(int i = 0; i < clouds.length; i++)
            clouds[i] = new Cloud(canvasHeight, canvasWidth, context, seed + (i * 38)); //seed controls how W I D E clouds are

    }

    public void draw(Canvas canvas, Paint paint) {
        for(int i = 0; i < clouds.length; i++)
            clouds[i].drawBitMap(canvas, paint);

    }

    public void updatePosition(double elapsed) {
        for(int i = 0; i < clouds.length; i++)
            clouds[i].updatePositionX(elapsed);
    }

    public void startClouds() {
        for(int i = 0; i < clouds.length; i++)
            clouds[i].go = true;
    }
}
